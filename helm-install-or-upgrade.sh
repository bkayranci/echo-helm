#!/usr/bin/env bash

export RELEASE_NAME="$1"
export ARGS=${@:2}
export DEPLOYS=$(helm ls | grep $RELEASE_NAME | wc -l)

if [ ${DEPLOYS}  -eq 0 ]; then helm install "${RELEASE_NAME}" $ARGS; else helm upgrade "${RELEASE_NAME}" $ARGS; fi
